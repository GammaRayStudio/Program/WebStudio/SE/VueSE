import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import CreateVueApp from '../views/CreateVueApp.vue'
import AttributeBinding from '../views/AttributeBinding.vue'
import ConditionalRending from '../views/ConditionalRending.vue'
import ListRendering from '../views/ListRendering.vue'
import EventHanding from '../views/EventHanding.vue'
import ClassStyleBinding from '../views/ClassStyleBinding.vue'
import ComputedProperties from '../views/ComputedProperties.vue'
import ComponentsProps from '../views/ComponentsProps.vue'
import CommunicatingEvents from '../views/CommunicatingEvents.vue'
import FormsVModel from '../views/FormsVModel.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/L2',
    name: 'Creating the Vue App',
    component: CreateVueApp
  },
  {
    path: '/L3',
    name: 'Attribute Binding',
    component: AttributeBinding
  },
  {
    path: '/L4',
    name: 'Conditional Rending',
    component: ConditionalRending
  },
  {
    path: '/L5',
    name: 'List Rendering',
    component: ListRendering
  },
  {
    path: '/L6',
    name: 'Event Handing',
    component: EventHanding
  },
  {
    path: '/L7',
    name: 'Class & Style Binding',
    component: ClassStyleBinding
  },
  {
    path: '/L8',
    name: 'Computed Properties',
    component: ComputedProperties
  },
  {
    path: '/L9',
    name: 'Components & Props',
    component: ComponentsProps
  },
  {
    path: '/L10',
    name: 'Communicating Events',
    component: CommunicatingEvents
  },
  {
    path: '/L11',
    name: 'Forms & v-model',
    component: FormsVModel
  }


]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
